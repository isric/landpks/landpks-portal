# Integration testing

Test cases are build in python using selenium, see https://selenium-python.readthedocs.io

You need to fetch [chromedriver](https://chromedriver.chromium.org/downloads) for your platform and place it in root of the project. Note that the version of chrome should metch with the one available on your platform.

Requires python+poetry

Run tests with:

```
poetry run pytest -s
```