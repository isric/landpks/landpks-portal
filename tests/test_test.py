from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.chrome.service import Service as ChromeService
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.support.select import Select
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import requests,json


def test_mapserver():
    r = requests.get("https://dev-ms-landpks.containers.wurnet.nl/ethiopia?&service=WMS&request=GetMap&layers=Woreda&styles=&format=image%2Fpng&transparent=true&version=1.1.1&height=1024&width=1024&srs=EPSG%3A3857&bbox=3757032.814272984,0,5009377.085697311,1252344.2714243277")

    assert r.status_code == 200
    assert r.headers['content-type'] == 'image/png'

    r = requests.get("https://dev-ms-landpks.containers.wurnet.nl/ethiopia?SERVICE=WMS&VERSION=1.3.0&REQUEST=GetFeatureInfo&BBOX=8.891300000000001,38.5114,8.90130.01,38.52140.01&CRS=EPSG:4326&WIDTH=100&HEIGHT=100&LAYERS=BulkDens,Corg,Kexch,Norg,Polsen,Ptot,pH,rzd,slope,CrsFrg,texture&FORMAT=image/tiff&QUERY_LAYERS=BulkDens,Corg,Kexch,Norg,Polsen,Ptot,pH,rzd,slope,CrsFrg,texture&INFO_FORMAT=application/vnd.ogc.gml&I=50&J=50")

    assert r.status_code == 200
    assert r.headers['content-type'] == 'application/vnd.ogc.gml; charset=UTF-8'

    r=None

def test_model():
    #valid call
    r = requests.get("https://dev-landpks-model.containers.wurnet.nl/yieldresponse/Maize/High?user=farmer&x=38.5214&y=8.9013&prizeMarket=24&costfNR=15&costfPR=20&costfKR=10")
    assert r.status_code == 200
    assert r.headers['content-type'] == 'application/json'
    assert 'yfertilized' in r.json()[0].keys()

    #out-of range
    r = requests.get("https://dev-landpks-model.containers.wurnet.nl/yieldresponse/Maize/High?user=farmer&x=138.5214&y=28.9013&prizeMarket=24&costfNR=15&costfPR=20&costfKR=10")
    assert r.status_code == 500
    assert 'error' in r.json().keys()

    r = requests.get("https://dev-landpks-model.containers.wurnet.nl/yieldresponse_hyper/Maize/High?user=farmer&x=38.5214&y=8.9013&prizeMarket=24&costfNR=15&costfPR=20&costfKR=10&Fallow=false&Leguminous=false&Mineral=false&Organicresources=Remained&slopepos=middle&cultivar=high-yielding&labor=mechanized&weedcontrol=optimal&swc=optimal&texture=S&BulkDens=&Corg=&Kexch=&Norg=&Polsen=&Ptot=&pH=&rzd=")
    assert r.status_code == 200
    assert r.headers['content-type'] == 'application/json'
    assert 'yfertilized' in r.json()[0].keys()

    r = requests.get("https://dev-landpks-model.containers.wurnet.nl/cropplanner/Maize/High?Woreda=Addis+Ketema&prizeMarket=24&costfNR=15&costfPR=20&costfKR=10")
    assert r.status_code == 200
    assert r.headers['content-type'] == 'application/json'
    print('uuid: ',r.json()[0][0])
    assert r.json()[0][0] != ''
    assert 'yfertilized' in r.json()[1][0].keys()

    r=None

def test_fieldapp():

    chrome_options = Options()
    chrome_options.add_argument("--headless")
    chrome_options.add_argument("--window-size=1920x1080")

    driver = webdriver. Chrome(chrome_options=chrome_options, executable_path="./chromedriver")

    #driver = webdriver.Chrome(service=ChromeService(executable_path=ChromeDriverManager().install()))

    driver.get("https://dev.slm-portal.containers.wurnet.nl/tools/cropfertilityfield/")
    #driver.get("https://localhost:1313/tools/cropfertilityfield/")

    title = driver.title
    assert title == "Fertilizer decision support at field level ( beta ) - Sustainable Land Management by ISRIC"

    driver.implicitly_wait(5)

    Select(driver.find_element(by=By.ID, value='crop')).select_by_value('Maize')
    Select(driver.find_element(by=By.ID, value='Management')).select_by_value('High')
    
    #print(Select(driver.find_element(by=By.ID, value='Management')).first_selected_option.text)

    # this requires some time to build up, else empty
    tb1 = driver.find_element(by=By.ID, value="lng")
    tb1.send_keys("38")
    tb2 = driver.find_element(by=By.ID, value="lat")
    tb2.send_keys("9");
    bt1 = driver.find_element(by=By.ID, value="btGo")
    driver.execute_script("arguments[0].click();", bt1)

    bt3 = driver.find_element(by=By.ID, value="btNext")
    # the button is off page, use script to trigger it
    driver.execute_script("arguments[0].click();", bt3)

    # make sure no popup is displayed
    elm = driver.find_element(By.CSS_SELECTOR, ".error-modal")
    if elm.is_displayed():
        print(driver.find_element(By.CSS_SELECTOR, ".model-txt").get_attribute("innerHTML"))
        assert False
    else:
        el = driver.find_element(by=By.XPATH, value="//table[@id='RecTxt']/tbody/tr[2]/td[1]")
        print(el.get_attribute("innerHTML"))
        assert el.get_attribute("innerHTML") != ''

        # if recommendation is 0 no graph will be displayed

        sb4 = driver.find_element(by=By.ID, value="btHyper")
        driver.execute_script("arguments[0].click();", sb4)
        
        # after hyperlocalise, the content should have 2 numbers split by /
        el = driver.find_element(by=By.XPATH, value="//table[@id='RecTxt']/tbody/tr[2]/td[1]/span")
        assert float(el.get_attribute("innerHTML")) > -1

        
    driver.quit()

def test_regionapp():

    chrome_options = Options()
    chrome_options.add_argument("--headless")
    chrome_options.add_argument("--window-size=1920x1080")

    driver = webdriver. Chrome(chrome_options=chrome_options, executable_path="./chromedriver")

    #driver = webdriver.Chrome(service=ChromeService(executable_path=ChromeDriverManager().install()))

    driver.get("https://dev.slm-portal.containers.wurnet.nl/tools/cropfertilityregion/")
    #driver.get("https://localhost:1313/tools/cropfertilityfield/")

    driver.implicitly_wait(30)

    Select(driver.find_element(by=By.ID, value='crop')).select_by_value('Maize')
    Select(driver.find_element(by=By.ID, value='Management')).select_by_value('High')

    bt3 = driver.find_element(by=By.ID, value="btNext")
    # the button is off page, use script to trigger it
    driver.execute_script("arguments[0].click();", bt3)

    # make sure no popup is displayed
    elm = driver.find_element(By.CSS_SELECTOR, ".error-modal")
    if elm.is_displayed():
        print(driver.find_element(By.CSS_SELECTOR, ".model-txt").get_attribute("innerHTML"))
        assert False
    else:
        el = driver.find_element(by=By.XPATH, value="//table[@id='RecTxt']/tbody/tr[2]/td[1]")
        print(el.get_attribute("innerHTML"))
        assert el.get_attribute("innerHTML") != ''

    

