---
title: 'Fertilizer decision support at District level ( beta )'
date: 2022-08-01T15:14:39+10:00
icon: '/images/region-app.png'
featured: true
draft: false
heroHeading: 'Fertilizer decision support at District level ( beta )'
heroSubHeading: 'cropfertility'
heroBackground: 'services/service2.jpg'
---

Welcome to the 1st version prototype of the online fertilizer decision support tool developed by the SOILS-Consortium (ISRIC, IFDC and USAID) for the Space to Place initiative. This tool generates NPK fertilizer recommendations for  major crops for, to be selected, fields in the croplands of target countries in sub-Saharan Africa, at three levels of agricultural management intensity knowing high, medium and low.

{{< map-select-region >}}
Welcome to the 1st version prototype of the online fertilizer decision support tool developed by the SOILS-Consortium (ISRIC, IFDC and USAID) for the Space to Place initiative. This tool generates NPK fertilizer recommendations for  major crops for, to be selected, fields in the croplands of target countries in sub-Saharan Africa, at three levels of agricultural management intensity knowing high, medium and low. The recommendations are based on different criteria including productivity (maximal yield gain or maximal fertiliser use efficiency) and economy (maximal net return or maximal net return cost ratio). For the latter purpose, grain market price can be specified per crop as well as the costs of NPK per fertilizer type. The results are provided in the form of maps showing the spatial variance within the selected District of the N, P and K recommendations (kg/ha) and the associated costs (price/ha) according to the selected criterium. The results are also presented in a table and displayed by response curves in a graph showing yield (kg/ha) as a function of N, P and K (kg/ha), and net return (price/ha) as a function of costs (price/ha), as an average (plus deviation) for the District croplands,  thereby also indicating the recommended average NPK rates and costs according to the selected criterium. Last, the total sum of N, P and K, and costs, required for the District cropland area is provided as a separate figure.  

The user can point to the map for a specific location and subsequently a second curve will be created showing the responses and recommendations for that specific point location.  

The calculations are based on what we call ‘spatial nutrient gap analysis’. Herein we calculate the attainable crop yield as a function of soil water availability and the associated crop nutrient demand as a function of crop parameters derived from fertilizer trial data. This nutrient demand we compare with the soil nutrient supply which we calculate from soil property maps and parameters derived from fertilizer trial data. Then we calculate the efficiency of fertilizer nutrients, again using parameters derived from fertilizer trial data, to fill the nutrient gap. The model train that we use for this purpose runs online (which may take a moment) following the user-sourced specifications.  

Please note that the current prototype is still limited in scope and it is intended to extend this during a follow up phase, including : 
<ul>
<li>Improvement of the soil-crop model train by inclusion of WOFOST (to estimate water-limited crop production) and INITATOR (to estimate soil and fertilizer (micro)nutrient availability) </li>
<li>Spatially variable model parameterization (pending additional and adequate trial data) </li>
<li>Inclusion of national soil map data </li>
<li>Inclusion of fertilizer source </li>
<li>Inclusion of multi-year variance in crop response efficiency </li>
<li>Inclusion of multi-field multi-crop farm optimization  </li>
<li>Inclusion of uncertainty propagation </li>
<li>Inclusion of soil health </li>
<li>Further work out of agricultural best practices </li>
</ul>


{{< /map-select-region >}}

{{<rw class="d-flex justify-content-between align-items-center bg-white rounded px-5">}}
{{<cl w="3" class="">}}
{{<fig src="/images/ISRIC_Website_LogoCard.png" title="Logo isric.org" class="w-100">}}
{{</cl>}}
{{<cl w="3" class="">}}
{{<fig src="/images/ifdc.png" title="Logo IFDC" class="w-100">}}
{{</cl>}}
{{<cl w="3" class="">}}
{{<fig src="/images/usaid.png" title="Logo USAID" class="w-100">}}
{{</cl>}}
{{</rw>}}