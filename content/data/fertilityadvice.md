---
title: "fertility advice"
author: "Johan Leenaars"
date: 2022-06-15
---

A fertility advice has been prepared for the selected region. Select a relevant layer in the layer widget (top right).

{{% mapml h="400px" x="40" y="9" z="6" %}}
{{% mapml-wms url=`https://dev-ms-landpks.containers.wurnet.nl/ethiopia?` layer=`BulkDens` label=`Bulk Density` checked=`True` %}}
{{% mapml-wms url=`https://dev-ms-landpks.containers.wurnet.nl/ethiopia?` layer=`Corg` label=`C org`  %}}
{{% mapml-wms url=`https://dev-ms-landpks.containers.wurnet.nl/ethiopia?` layer=`CrsFrg` label=`Coarse fragments`  %}}
{{% mapml-wms url=`https://dev-ms-landpks.containers.wurnet.nl/ethiopia?` layer=`Kexch` label=`Potassium`  %}}
{{% mapml-wms url=`https://dev-ms-landpks.containers.wurnet.nl/ethiopia?` layer=`Norg` label=`Nitrogen`  %}}
{{% mapml-wms url=`https://dev-ms-landpks.containers.wurnet.nl/ethiopia?` layer=`OrgC` label=`Organic C` %}}
{{% mapml-wms url=`https://dev-ms-landpks.containers.wurnet.nl/ethiopia?` layer=`pHH2O` label=`pH H2O`  %}}
{{% mapml-wms url=`https://dev-ms-landpks.containers.wurnet.nl/ethiopia?` layer=`Polsen` label=`P Olsen`  %}}
{{% mapml-wms url=`https://dev-ms-landpks.containers.wurnet.nl/ethiopia?` layer=`Ptot` label=`P total`  %}}
{{% mapml-wms url=`https://dev-ms-landpks.containers.wurnet.nl/ethiopia?` layer=`rzd` label=`Rootzone depth`  %}}
{{% /mapml %}}

