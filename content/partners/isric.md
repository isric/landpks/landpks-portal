---
title: 'ISRIC - World Soil Information'
date: 2018-11-28T15:14:39+10:00
icon: '/images/ISRIC_Website_LogoCard.png'
featured: true
weight: 1
draft: false
heroHeading: 'ISRIC - World Soil Information'
heroSubHeading: 'ISRIC - World Soil Information is an independent foundation and a custodian of global soil information which they produce, gather, compile and serve together with our partners at global, national and regional levels.'
heroBackground: 'services/service2.jpg'
---



<div class="row">

  <div class="col-12 col-md-6 mb-2">
    <div class="team team-summary team-summary-large">
      <div class="team-image">
        <img  alt="Photo of André Kooiman" class="img-fluid mb-2" src="/images/team/Andre.jpg">
      </div>
      <div class="team-meta">
        <h2 class="team-name">André Kooiman</h2>
        <p class="team-description">Senior Specialist Sustainable Land Management</p>
      </div>
      <div class="team-content">    
        <p>André Kooiman is an expert in soil and water conservation and climate change, land use planning, geo-information management and earth observation in international development. He has over 30 years of experience in project management and acquisition, applied research, capacity building and education in Africa, Southeast Asia, Pacific and Eastern Europe and the Netherlands.</p>
      </div>
    </div>
  </div>

  <div class="col-12 col-md-6 mb-2">
    <div class="team team-summary team-summary-large">
      <div class="team-image">
        <img  alt="Photo of Johan Leenaars" class="img-fluid mb-2" src="/images/team/johan_leenaars.jpg">
      </div>
      <div class="team-meta">
        <h2 class="team-name">Johan Leenaars</h2>
        <p class="team-description">Senior Soil Scientist</p>
      </div>
      <div class="team-content">
        <p>Johan joined ISRIC in 2009 as Soil Legacy Data Officer for the GlobalSoilMap.net, Africa Soil Information Service (AfSIS) and later the WoSIS projects. More recently, Johan is focusing his work at ISRIC on soil fertility projects. His expertise is in soil science, agriculture, geo-information and Africa. He graduated from Wageningen University in 1990 with a specialization in soil survey and land evaluation. Since he has been involved in projects on soil & land resources and worked for public organizations, commercial companies as well as freelance.</p>
      </div>
    </div>
  </div>
  
</div>



