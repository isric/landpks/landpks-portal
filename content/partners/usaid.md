---
title: 'USAID'
date: 2018-11-28T15:14:39+10:00
icon: '/images/usaid.png'
featured: true
weight: 2
draft: false
heroHeading: 'USAID'
heroSubHeading: 'USAID leads international development and humanitarian efforts to save lives, reduce poverty, strengthen democratic governance and help people progress beyond assistance.'
heroBackground: 'services/service2.jpg'
---





