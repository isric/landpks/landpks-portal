---
title: 'SILC - University of Colorado Boulder'
date: 2018-11-28T15:14:39+10:00
icon: '/images/Boulder.jpg'
featured: true
weight: 2
draft: false
heroHeading: 'SILC - University of Colorado Boulder'
heroSubHeading: 'SILC develops the knowledge, capacity, partnerships, technology, and diverse workforce to accelerate the transition to a more sustainable world and educates the next generation of researchers, communicators, and innovators who will lead this change. SILC supports scholars and students engaged in impact-oriented research, education, and service in order to improve the success and sustainability of human activities.'
heroBackground: 'services/service2.jpg'
---

