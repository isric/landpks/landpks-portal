---
title: 'IFDC'
date: 2018-11-28T15:14:39+10:00
icon: '/images/ifdc.png'
weight: 2
featured: true
draft: false
heroHeading: 'International Fertilizer Development Center'
heroSubHeading: 'IFDC is an independent non-profit organization that combines innovative research, market systems development, and strategic partnerships to spread sustainable agricultural solutions for improved soil health, food security, and livelihoods around the world.'
heroBackground: 'services/service2.jpg'
---


