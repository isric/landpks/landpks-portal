---
title: 'Ethiopian Institute of Agricultural Research (EIAR)'
date: 2018-11-28T15:15:34+10:00
icon: '/images/EIAR_website_LogoCard.png'
featured: true
weight: 5
draft: false
heroHeading: 'Ethiopian Institute of Agricultural Research (EIAR)'
heroSubHeading: 'As a national research institute, Ethiopian Institute of Agricultural Research (EIAR) aspires to see improved livelihood of all Ethiopians engaged in agriculture, agro-pastoralism, and pastoralism through market-competitive agricultural technologies.'
heroBackground: 'services/service2.jpg'
---


