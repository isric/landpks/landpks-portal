# Documentation about the DST App

The documentation for this app has 3 parts:

- [Hugo CMS](hugo.md) is used as a framework around the app, for branding and documentation purposes
- [jquery-leaflet-app](jquery-leaflet-app.md) describes the app itself
- [deployment](deployment.md) describes how the app can be deployed