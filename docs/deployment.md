# Deployment

The deployment of the app is based on the deployment of all ISRIC Hugo websites. At each push to the GIT repository a 
[CI-CD process](../.gitlab-ci.yml) is triggered, which builds a container image with the new html content. This image is then deployed to our kubernetes infrastructure.