# HUGO CMS

[HUGO](https://gohugo.io) is a Content Management System which is able to build a 'static' html website from a repository of [markdown](https://en.wikipedia.org/wiki/Markdown) resources. The markdown assets are typically captured in a GIT repository. The style of the website is defined in a hugo-theme. At ISRIC we have selected a [bootstrap](https://getbootstrap.com) based theme to align with our other websites. 

## Updating content on Hugo

Changes to the website are possible at 2 levels:
- the content level; markdown files in the [content folder](../content) contain the actual textual content of the CMS. Images are placed in the [static folder](../static/images). Notice that each markdown file contains a header with some parameters like title and author, these parameters are used by hugo to categorize the content. The folder structure in the content folder follows the structure of the website.
- the layout/theme level; changes to these files are best done by a web developer. You either work directly on the theme, but preferably duplicate the file to the main app and change it there (files in theme replace files in the main app if they don't exist).

Changes to the content need to be pushed to the relevant branch on git, either directly or via a Pull Request (if you want to ask a review by someone).

It is possible to run hugo locally. Install hugo preferably from a package maneger like [chocolatery](https://chocolatey.org/) (windows), [homebrew](https://brew.sh/) or [apt](https://en.wikipedia.org/wiki/APT_(software)) (linux). Navigate to the folder on which you cloned the repository and run: 

```
hugo serve
```

A development server will spin up and you can access the site locally via http://localhost:1313.



