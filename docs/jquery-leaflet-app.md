# JQUERY Leaflet APP

The fertiliser recommendation app is integrated with the [HUGO CMS](hugo.md), although it can easily be extracted to run seperately. There is however a strong dependency on the bootstrap framework. The app is integrated in hugo as a [shortcode](../layouts/shortcodes/map-select-field.html) in combination with a [javascript file](../static/js/model.js). 

The platform contains 2 apps, one for field level analyses and one for area level.

## model parameters and outputs

The workflow of data in the field level app has the following phases
- Identify a location (user input or current location), this is combined with some common parameters, such as crop and management intensity.
- Get default values for this location, either from configuration or from a call to a series of SoilGrids layers (mapserver featureinfo)
- Run the model with the default parameters
- Visualisation of the model results
- Option to finetune some of the input parameters
- Rerun the model with updated parameters
- Visualize the rerun outputs in comparison to the initial parameters
- Each location is stored in a locationlist, previous locations can be re-opened or removed from the list

The workflow of data in the area level app has the following phases
- Identify an area (user input), this is combined with some common parameters, such as crop and management intensity.
- Run the model with the initial parameters, derived from SoilGrids layers (at model side)
- Visualisation of the model results
- Option to select a location on the modelled area
- Rerun the model for the location
- Visualize the rerun outputs in comparison to the area output

## javascript libraries

Libraries used in the javascript app are:
- jquery & bootstrap facilitate to app development
- leaflet introduces map visualization
- bing.js adds bing background tiles to leaflet
- georaster & georater-for-leaflet add direct raster reading capabilities to leaflet
- chartjs introduce graph visualisation
- chartjs-error-bars adds error bard to chartjs
- popper is used by bootstrap to introduce nice help tooltips

## translations

Many strings in the app are configurable by a [translation file](../static/js/trans.js). If strings are hardcoded in the shortcode, then they can't be translated via this mechanism. In next iterations we can add a mechanism to select a relevant translation file based on the browser language or selected country.