## [1.3.2](https://git.wur.nl/isric/landpks/landpks-portal/compare/1.3.1...1.3.2) (2024-08-05)


### Bug Fixes

* update lightppd ([fb62af3](https://git.wur.nl/isric/landpks/landpks-portal/commit/fb62af3925f02e302b897906df0ca87dc27c957f))

## [1.3.1](https://git.wur.nl/isric/landpks/landpks-portal/compare/1.3.0...1.3.1) (2024-02-01)


### Bug Fixes

* prepare release ([6f90e09](https://git.wur.nl/isric/landpks/landpks-portal/commit/6f90e096dec6e5dac305fa0f39c3f5b91cea4a26))

## [1.3.0](https://git.wur.nl/isric/landpks/landpks-portal/compare/1.2.2...1.3.0) (2023-10-17)


### Features

* zambia release ([2a3b1b8](https://git.wur.nl/isric/landpks/landpks-portal/commit/2a3b1b8a4b456236df19ad0c00e0b878e5360645))

## [1.2.2](https://git.wur.nl/isric/landpks/landpks-portal/compare/1.2.1...1.2.2) (2023-09-05)


### Bug Fixes

* remove non s2p apps ([aaae82f](https://git.wur.nl/isric/landpks/landpks-portal/commit/aaae82ff37002eabe1770f48784dc0da5d641807))
* update CI ([120bc45](https://git.wur.nl/isric/landpks/landpks-portal/commit/120bc456a124d677cbfa6a3b1a2cc8784a38dff4))

## [1.2.1](https://git.wur.nl/isric/landpks/landpks-portal/compare/1.2.0...1.2.1) (2023-06-23)


### Bug Fixes

* release 26 june 2023 ([733b435](https://git.wur.nl/isric/landpks/landpks-portal/commit/733b43504b5e4a93eecda4c3be2881c929197a2d))

## [1.2.0](https://git.wur.nl/isric/landpks/landpks-portal/compare/1.1.0...1.2.0) (2023-05-31)


### Features

* malawi release ([9bcd329](https://git.wur.nl/isric/landpks/landpks-portal/commit/9bcd3294f7738d99a05837ae34719ef9ad8d6466))

## [1.1.0](https://git.wur.nl/isric/landpks/landpks-portal/compare/1.0.5...1.1.0) (2023-05-17)


### Bug Fixes

* new release fixes some bugs, it adds a mask layer to non arable land ([1643ebb](https://git.wur.nl/isric/landpks/landpks-portal/commit/1643ebb3ea4e184a83199cdf989de42966855d59))


### Features

* feature release; madagascar cycle ([d394d9e](https://git.wur.nl/isric/landpks/landpks-portal/commit/d394d9e12d80956ccc6861cf74db1de845c60b58))

## [1.0.5](https://git.wur.nl/isric/landpks/landpks-portal/compare/1.0.4...1.0.5) (2023-02-20)


### Bug Fixes

* make release ([a431baa](https://git.wur.nl/isric/landpks/landpks-portal/commit/a431baa41d2796ee3d418917a4d8ad5e0633f025))

## [1.0.4](https://git.wur.nl/isric/landpks/landpks-portal/compare/1.0.3...1.0.4) (2023-02-13)


### Bug Fixes

* new minor, help buttons on main form ([c2c7d32](https://git.wur.nl/isric/landpks/landpks-portal/commit/c2c7d3293f48ffd41673660d52e4c07c1e54cb22))

## [1.0.3](https://git.wur.nl/isric/landpks/landpks-portal/compare/1.0.2...1.0.3) (2023-02-13)


### Bug Fixes

* add niger/madagascar ([8512ca5](https://git.wur.nl/isric/landpks/landpks-portal/commit/8512ca50606d3ef1bdc45b98289784b0291a22a2))

## [1.0.2](https://git.wur.nl/isric/landpks/landpks-portal/compare/1.0.1...1.0.2) (2023-02-09)


### Bug Fixes

* also build tags ([e42a30d](https://git.wur.nl/isric/landpks/landpks-portal/commit/e42a30d2786db6f1d110fb05baa13d337aa7a175))

## [1.0.1](https://git.wur.nl/isric/landpks/landpks-portal/compare/1.0.0...1.0.1) (2023-02-09)


### Bug Fixes

* ci ([60bf972](https://git.wur.nl/isric/landpks/landpks-portal/commit/60bf97264de1974ebe467264dc272c24c3d5d69d))

## [1.0.0](https://git.wur.nl/isric/landpks/landpks-portal/compare/...1.0.0) (2023-02-09)


### Bug Fixes

* fix release ([c60e30a](https://git.wur.nl/isric/landpks/landpks-portal/commit/c60e30a147754bb1fc28a64bb9552ccd688216c7))


### Features

* initial release ([b2e0451](https://git.wur.nl/isric/landpks/landpks-portal/commit/b2e04514e0f1d5019d04e583f079c33e22db4eac))
* use space2place layout ([f2608f6](https://git.wur.nl/isric/landpks/landpks-portal/commit/f2608f6534f28d581792dd06f2bd1c5562b2e845))
