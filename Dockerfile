FROM harbor.containers.wurnet.nl/proxy-cache/klakegg/hugo:0.111.3-ext-ubuntu-onbuild AS hugo

FROM harbor.containers.wurnet.nl/proxy-cache/rtsp/lighttpd:1.4.76
COPY --from=hugo /target /var/www/html/

COPY 01-server.conf /etc/lighttpd/conf.d/01-server.conf

EXPOSE 8080/tcp

ENTRYPOINT ["/usr/sbin/lighttpd"]
CMD ["-D", "-f", "/etc/lighttpd/lighttpd.conf"]