var countries = {
    "Madagascar": { 
        "center": [47.3, -18.5], 
        "label": "madagascar", 
        "layer": "ADM3", 
        "crops": ["Sorghum","Maize","Beans","Rice","Cassava"], 
        "prices": {
            "Maize": 240,
            "Sorghum": 250,
            "Wheat": 300,
            "Teff": 420,
            "Barley": 230,
            "Beans": 200,
            "Rice": 200,
            "Cassava": 200,
            "costfKR": 100,
            "costfNR": 150,
            "costfPR": 200
        }, 
        "trans": { "coins": "Ar" } },
    "Ethiopia": { 
        "center": [9, 40], 
        "label": "ethiopia", 
        "layer": "Woreda", 
        "crops": ["Maize", "Sorghum", "Wheat", "Barley"], 
        "prices": {
            "Maize": 24,
            "Sorghum": 25,
            "Wheat": 30,
            "Teff": 42,
            "Barley": 23,
            "Beans": 20,
            "Rice": 20,
            "Cassava": 20,
            "costfKR": 10,
            "costfNR": 15,
            "costfPR": 20
        }, 
        "trans": { "coins": "Br" } },
    "Niger": { 
        "center": [7, 14], 
        "label": "niger", 
        "layer": "ADM3", 
        "crops": ["Maize", "Sorghum"], 
        "prices": {
            "Maize": 2.4,
            "Sorghum": 2.5,
            "Wheat": 3.0,
            "Teff": 4.2,
            "Barley": 2.3,
            "Beans": 2.0,
            "Rice": 2.0,
            "Cassava": 2.0,
            "costfKR": 1.0,
            "costfNR": 1.5,
            "costfPR": 2.0
        }, 
        "trans": { "coins": "Fr" } }
}
