
if (typeof app === 'undefined') app = {};
app.lyrs = ["BulkDens", "Corg", "Kexch", "Norg", "Polsen", "Ptot", "pH", "rzd", "slope", "CrsFrg", "texture"]; // ymax, lgp
app.marker = {};
app.regionMarker = {};
app.countries = [];
app.currentCountry = '';
app.districts = {};
app.data = {};
app.map = {};
app.fertMap = {};
app.layer = {};
app.markerHistory;
app.graphData = [];
app.detailData = [];
app.probabilityData = [];
app.aGraph = {};
app.farmFields = {};
app.farms = {};
app.farm = {};
app.defaults = {
    "Fallow": false,
    "Leguminous": false,
    "Mineral": false,
    "Organicresources": "Remained",
    "reason-depth-restriction": "",
    "slopepos": "middle"
};
app.total = 0;
app.colors = ["#7a0403", "#a91601", "#d02f05", "#eb500e", "#fb7e21", "#fdac34", "#eecf3a", "#cdec34", "#a4fc3c", "#6dfe62", "#32f298", "#18ddc2", "#3366cc"];
defaultPrices = {};

app.login = function () {
    $('#loading').show();
    $.ajax({
        type: "get",
        url: app.modelurl + 'countryconfiguration/countries',
        timeout: 5000,
        beforeSend: function (xhr) {
            xhr.setRequestHeader("Authorization", "Basic " + btoa($('#us').val() + ":" + $('#pw').val()));
        },
        success: function (json) {
            $('#login').hide();
            $('#app').show();
            app.countries = {}
            json.forEach(c => {
                app.countries[c] = {};
                $('#country').append('<option value="' + c + '">' + app.tr(c) + '</option>')
                app.currentCountry = c;
            })
            app.setCountry($('#country').val());
            $('#theapp').show();
            app.map.invalidateSize();
            $('#beta-warning').hide();
        },
        error: function (json, status, error) {
            if (status == "timeout") {
                alert("Server unavailable, check your internet connection else contact administrator");
            } else if (json.status == 503) {
                alert("S2P backend currently unavailable, contact administrator");
            } else {
                alert("Login failed, verify your credentials with the site administrator");
            }
        },
        complete: function () {
            //we hide loading after regions load
        }
    });
}

//triggers when a province is changed, populates the district's of selected province
app.provinceChange = function (val) {
    //empty Chapters- and Topics- dropdowns
    $("#district").find('option').remove().end();
    ws = Object.keys(app.districts[val]).sort();
    for (var i in ws) {
        $("#district").append($('<option>', {
            value: ws[i],
            text: ws[i]
        }));
    }
    //zoom initial value
    app.zoomdistrict($("#district option:selected").val());
}

app.getRegions = function (cntry) {
    app.districts = [];
    //has structure [[adm3,adm2,adm1,x1,x2,y1,y2],[]]
    app.countries[cntry].regions.forEach(c => {
        if (!app.districts[c[2]]) app.districts[c[2]] = {};
        app.districts[c[2]][c[0]] = app.toBounds(c.slice(-4));
    })
    $("#province").find('option').remove().end();
    for (const [key, value] of Object.entries(app.districts).sort()) {
        $("#province").append($('<option>', {
            value: key,
            text: key
        }));
    }
    app.provinceChange($("#province").val())
}

app.toBounds = function (arr) {
    bnds = L.latLngBounds([[arr[2], arr[0]], [arr[3], arr[1]]]);
    return bnds;
}

app.setCountry = function (cntry) {
    if (cntry && cntry != '' && cntry != app.currentCountry) {
        app.currentCountry = cntry;
        $('#loading').show();
        $.ajax({
            type: "get",
            url: app.modelurl + 'countryconfiguration/' + app.currentCountry,
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Authorization", "Basic " + btoa($('#us').val() + ":" + $('#pw').val()));
            },
            success: function (json) {
                app.countries[cntry] = json;
                app.map.fitBounds(app.toBounds(app.countries[cntry]['bounds']));
                app.getRegions(cntry);
                //remove the border layer and add it again
                app.map.removeLayer(wmsLayer);
                //strLayer = app.countries[cntry]['layer'];
                if (app.app == 'region') {
                    app.map.removeLayer(wmsLayer2);
                    wmsLayer2 = L.tileLayer.wms(app.wmsurl + cntry, { layers: 'adm3', transparent: true, format: "image/png", tileSize: 1024 }).addTo(app.fertMap);
                } else {
                    app.map.removeLayer(wmsMaskLayer);
                    wmsMaskLayer = L.tileLayer.wms(app.wmsurl + cntry, { layers: 'mask', opacity: .5, transparent: true, format: "image/png", tileSize: 1024 }).addTo(app.map);
                }
                wmsLayer = L.tileLayer.wms(app.wmsurl + cntry, { layers: 'adm3', transparent: true, format: "image/png", tileSize: 1024 }).addTo(app.map);
                /*load translation overrides*/
                if (app.countries[cntry]['translation'])
                    for (const [k, v] of Object.entries(app.countries[cntry]['translation'])) {
                        trans[k] = v;
                    }
                currentCrop = $('#crop').val();
                app.setCrops('#crop', cntry);
                $('#crop').val(currentCrop).change();
                if (app.app == 'farm') {
                    app.setCrops('select.crop', cntry);
                }
                app.setFertilizers('#ferttype', cntry);
                $('.tr-coin').html(app.tr('coins'));
                app.loadDefaultPrices();
            },
            error: function (json, status, error) {
                alert("Failed to retrieve country " + cntry + ".");
            },
            complete: function () {
                $('#loading').hide();
            }
        })

    }
}

app.setCrops = function (sel, cntry) {
    $(sel).find('option').remove().end();
    $(sel).append($('<option>', {
        value: '',
        text: app.tr('Select crop')
    }));
    //load country specific crops
    if (cntry && cntry != '' && app.countries && app.countries[cntry] && app.countries[cntry].crops) {
        Object.keys(app.countries[cntry].crops).forEach(k => $(sel).append('<option value="' + k + '">' + app.tr(k) + '</option>'));
    } else {
        //console.log('mycr', app.countries);
    }
}

app.setFertilizers = function (sel, cntry) {
    $(sel).find('option').remove().end();
    if (cntry && cntry != '' && app.countries && app.countries[cntry] && app.countries[cntry].fertilisers) {
        Object.entries(app.countries[cntry].fertilisers).forEach(e => {
            const [k, v] = e;
            $(sel).append('<option value="' + k + '">' + (v.name || app.tr(k)) + '</option>')
        });
    }
}
app.modelRegion = function () {
    //new calc, so reset detaildata
    app.graphData = [];
    app.detailData = [];
    app.probabilityData = [];

    if (!$('#crop option:selected').attr("value")) {
        app.error('select-crop');
        return false;
    } else if (!$('#Management option:selected').attr("value")) {
        app.error('select-managent');
        return false;
    } else if (!$('#district option:selected').attr("value")) {
        app.error('select-region');
        return false;
    }

    req = {
        woreda: $('#district option:selected').val(),
        country: $('#country option:selected').val(),
        prizeMarket: $('#prizeMarket').val(),
        costfNR: $('#costfNR').val(),
        costfPR: $('#costfPR').val(),
        costfKR: $('#costfKR').val()
    }
    Modelurl = app.modelurl + 'cropplanner/' + $('#crop option:selected').val() + '/' + $("#Management option:selected").val();
    app.slide();
    $.ajax({
        type: "get",
        url: Modelurl,
        data: req,
        dataType: "json",
        beforeSend: function (xhr) {
            xhr.setRequestHeader("Authorization", "Basic " + btoa($('#us').val() + ":" + $('#pw').val()));
        },
        success: function (json) {
            $(".progress").hide();
            if (json.error) {
                app.error('Error: ' + json.message[0]);
            } else {
                uuid = json[0];
                $("#uuid").val(uuid);
                app.parseRegionRecommandation(json[1]);
                app.activeTab('Maps');
                app.fertMap.invalidateSize();
                app.setRegionMap();
            }
        },
        error: function (json, status, error) {
            $(".progress").hide();
            switch (status) {
                case 404:
                    app.error("not-found-error")
                    break;
                case 504:
                    app.error("timeout-error")
                    break;
                default:
                    try {
                        app.error('Error: ' + json.responseJSON.message[0]);
                    } catch (e) {
                        app.error("online-error")
                    }
            }
            app.activeTab('Configuration');
        }
    });
}

//add new layers by uuid
app.setMap = function (layer) {
    app.total = 0;
    if ($('#uuid').val() != '') {
        if (app.layer != {}) {
            app.fertMap.removeLayer(app.layer)
        }

        //in some cases the map has not initialised yet
        app.fertMap.invalidateSize();

        var url_to_geotiff_file = app.dataurl + $('#uuid').val() + '/' + $('#crop option:selected').val() + '_' + layer + '_' + $('#recbasis option:selected').val() + '.tif';

        fetch(url_to_geotiff_file)
            .then(response => response.arrayBuffer())
            .then(arrayBuffer => {
                parseGeoraster(arrayBuffer).then(georaster => {
                    app.mapLegend(georaster);
                    //console.log(georaster);
                    app.layer = new GeoRasterLayer({
                        georaster: georaster,
                        pixelValuesToColorFn: function (values) {
                            return app.colorMapPixel(values, georaster)
                        },
                        opacity: 0.7
                    }).addTo(app.fertMap);
                    app.fertMap.fitBounds(app.layer.getBounds());
                });
            });
    } else {
        app.error('run-model');
    }
}

app.mapLegend = function (georaster) {
    var range = parseFloat(georaster.maxs) - parseFloat(georaster.mins);
    var mn = parseFloat(georaster.mins);
    if (range < 8) range = 8;
    var lgnd = "<div><b>Legend (" + ($("input[name='fert']").val() == 'fertCost' ? app.tr('coins') : 'kg') + "/ha)</b></div><br/>";
    for (i = 0; i < app.colors.length; i++) {
        lgnd += "<div class='mr-1 legendItem' style='background-color:" + app.colors[i] + "'></div>" + (i == 0 ? "" : "< ") + (Math.floor(mn + (range / app.colors.length * i))) + '<br/>';
    }
    $('.mapLegend').html(lgnd);
}

app.colorMapPixel = function (values, georaster) {
    //if (values[0] == 0) return '#7a0403';
    if (values[0] == null || ["Infinite", "NaN", "nan", -99].includes(values[0])) return null;
    var range = parseFloat(georaster.maxs) - parseFloat(georaster.mins);
    if (range == 0) range = 1;
    app.total += (parseFloat(values[0]) > 0 ? parseFloat(values[0]) : 0);
    return app.colors[Math.ceil((parseFloat(values[0]) - parseFloat(georaster.mins)) / range * app.colors.length)];
}

app.error = function (txt) {
    $('.error-modal').modal();
    $('.model-txt').html(app.tr(txt));
}

app.zoomdistrict = function (val) {
    if (app.app != 'farm') {
        if (app.districts[$('#province option:selected').val()][val]){
            app.map.fitBounds(app.districts[$('#province option:selected').val()][val]);
        }
    }
}

app.slide = function () {
    /* triggers a timer element */
    $(".progress").show();
    var percentVal = 1;
    var id = setInterval(frame, 1000);

    function frame() {
        if (percentVal >= 100) {
            clearInterval(id);
        } else {
            percentVal += 3;
            $(".progress-bar").css("width", percentVal + '%').attr("aria-valuenow", percentVal + '%').text(percentVal + '%');
        }
    }
}
app.getParams = function (latlng, res) {
    /* this function populates hyperlocalise fields from defaults, 
    for some params, it needs to select a relevant class, 
    app.defaults is used to capture the original value, if the class is not altered */
    $(app.lyrs).each(function (i, lyr) {
        if (res && res[lyr]) {
            val = res[lyr];
            if (['slope', 'CrsFrg'].includes(lyr)) {
                $("input[name=" + lyr + "]").each(function () {
                    var [mn, mx] = this.value.split('-');
                    if (val > parseFloat(mn) && val <= parseFloat(mx)) {
                        $(this).attr('checked', 'checked');
                        app.defaults[lyr] = this.value;
                    }
                })
            } else if (lyr == 'texture') {
                TXTCLS = ["C", "SiC", "SC", "CL", "SiCL", "SCL", "L", "SiL", "SL", "Si", "LS", "S", "HC"];
                //TXTCLS = ["S", "LS", "SL", "SiL", "Si", "L", "SCL", "CL", "SiCL", "SC", "SiC", "C", "HC"];
                $('#' + lyr).val(TXTCLS[parseFloat(val) - 1]);
                app.defaults[lyr] = TXTCLS[parseFloat(val) - 1];
            } else {
                $('#' + lyr).val(Math.round(val * 10) / 10);
                app.defaults[lyr] = $('#' + lyr).val();
            }
        }
    })
}

app.fetchFieldRecommandation = function () {
    //new calc, so reset detaildata
    app.graphData = [];
    app.detailData = [];
    app.probabilityData = [];

    if (!$('#crop option:selected').attr("value")) {
        app.error('select-crop')
    } else if (!$('#Management option:selected').attr("value")) {
        app.error('select-management')
    } else if (!$('#lng').val() || !$('#lat').val()) {
        app.error('select-field')
    } else {
        //reset form to defaults
        $('#frmHyper').trigger("reset");
        app.setDefaultVals($('#Management option:selected').val());
        //trigger timer
        app.slide();
        try {
            app.aGraph.destroy();
        } catch (e) { }
        app.fetchPointRecommandation('field');
        app.archivePoint();
        app.activeTab('Results');
    }
}

//the function checks if a param is the default value, then returns empty string
app.getVal = function (key, val) {
    if (app.defaults[key] && app.defaults[key] == val) {
        return '';
    } else {
        return val;
    }
}

app.fetchPointRecommandation = function (recmode) {

    //reset previous
    app.detailData = [];

    if (!recmode) recmode = 'field';
    mode = recmode == "hyperlocalise" ? "yieldresponse_hyper" : "yieldresponse_hyper";
    if ($('#lng').val() != '') {
        Modelurl = app.modelurl + mode + '/' + $('#crop option:selected').val() + '/' + $('#Management option:selected').val();
        req = {
            'country': $('#country option:selected').val(),
            'x': $('#lng').val(),
            'y': $('#lat').val(),
            'prizeMarket': $('#prizeMarket').val(),
            'costfNR': $('#costfNR').val(),
            'costfPR': $('#costfPR').val(),
            'costfKR': $('#costfKR').val()
        }
        if (recmode == "hyperlocalise") {
            //checkboxes with no preset
            ["Fallow", "Leguminous", "Mineral"].forEach(i => req[i] = app.getVal(i, $('#' + i).prop("checked")));
            //radios with no preset 
            ["Organicresources", "reason-depth-restriction", "slopepos"].forEach(i => req[i] = app.getVal(i, $('#' + i + ':checked').val()));
            //selects with no preset
            ["cultivar", "labor", "weedcontrol", "swc", "texture"].forEach(i => req[i] = app.getVal(i, $('#' + i + ' option:selected').val()));
            //check or text boxes with preset 
            ['slope', 'CrsFrg'].forEach(l => req[l + 'Class'] = app.getVal(l, $('#' + l + ':checked').val()));
            //for other layers
            app.lyrs.forEach(l => !['slope', 'CrsFrg'].includes(l) ? req[l] = app.getVal(l, $('#' + l).val()) : null);
        }
        $.ajax({
            type: "GET",
            url: Modelurl,
            data: req,
            dataType: "json",
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Authorization", "Basic " + btoa($('#us').val() + ":" + $('#pw').val()));
            },
            success: function (json) {
                app.parsePointRecommandation(json, recmode);
                $('.progress').hide();
            },
            error: function (jq, err, t) {
                try {
                    app.error(jq.responseJSON.message[0]);
                } catch (e) {
                    app.error('error')
                }
                $('.progress').hide();
                if (recmode == 'field') {
                    app.activeTab('Configuration');
                }
            }
        });
    } else {
        app.error('select-field')
    }
}

app.fetchHyperLocalPointRecommandation = function () {
    app.fetchPointRecommandation('hyperlocalise');
}

app.archivePoint = function () {
    /* persists a point configuration to browser cache, to be displayed on 'location' tab
       only if the name is unique, else overwrites the previous
    */
    label = $("#label").val();
    if (label == '') label = 'None';
    var item = {};
    ["lat", "lng", "label", "size", "budget", "prizeMarket", "costfNR", "costfKR", "costfPR"].forEach(
        i => item[i] = $("#" + i).val());
    ["crop", "Management", "ferttype", "country"].forEach(
        i => item[i] = $('#' + i).val()); //todo ferts can be multiple
    item.cdate = new Date().toISOString().slice(0, 10)
    items = JSON.parse(localStorage.getItem('history')) || {};
    // reset from previous version
    if (Array.isArray(items)) items = {};
    items[label] = item;

    localStorage.setItem('history', JSON.stringify(items));
    app.plotHistory(items);
}

app.histOpen = function (label) {
    j = JSON.parse(localStorage.getItem('history'))[label];
    $("#country").val(j['country']).change();
    ["lat", "lng", "label", "size", "budget", "prizeMarket", "costfNR", "costfKR", "costfPR"].forEach(i => $("#" + i).val(j[i]));
    ["crop", "Management", "ferttype"].forEach(i => $("#" + i).val(j[i]).change());
    app.activeTab('Configuration');
    app.zoom();
}

app.histDel = function (label) {
    items = JSON.parse(localStorage.getItem('history')) || {};
    delete items[label]
    localStorage.setItem('history', JSON.stringify(items));
    app.plotHistory(items);
}

app.plotHistory = function (items) {
    if (!items) items = JSON.parse(localStorage.getItem('history')) || {};
    recs = "";
    var fg = L.featureGroup();
    for (const [key, h] of Object.entries(items)) {
        try {
            btns = "<td><button class='btn btn-sm btn-info' onclick=\"app.histOpen('" + key + "')\">Open</button>" +
                "<button class='btn btn-sm btn-danger ml-1' onclick=\"app.histDel('" + key + "')\">Remove</button></td>";
            recs = recs + "<tr><td>" + key + "</td><td>" + h.crop + "</td><td>" + h.size +
                "</td><td>" + h.budget + "</td><td>" + h.cdate + "</td>" + btns + "</tr>";
            // create the marker
            var marker = L.marker([h.lat, h.lng]).bindPopup(key);
            fg.addLayer(marker);
        } catch (e) { console.log(e) }
    };
    fg.addTo(app.histMap);
    app.histMap.fitBounds(fg.getBounds());
    $("#historyPanel").html(recs);
}

app.setPointGraph = function (fert) {
    if (!fert) fert = $("input[name='fert']").val();
    else $("input[name='fert']").val(fert);
    app.setGraph(fert);
}

app.setRegionGraph = function (fert) {
    if (!fert) fert = $("input[name='fert']").val();
    else $("input[name='fert']").val(fert);
    app.setGraph(fert);
}

app.setRegionMap = function (fert) {
    if (!fert) fert = $("input[name='fert']").val();
    else $("input[name='fert']").val(fert);
    recbasis = $('#recbasis option:selected').val();
    app.setMap(fert, recbasis);
    app.setGraph(fert);
    $('#mapTitle').html(app.tr(fert) + ' at ' + app.tr(mode) + '.');
    setTimeout(() => {
        $('#mapLayerHelp').html('Map displays the ' + app.tr(fert) +
            ' application recommendation based on the selected recommendation basis: ' +
            app.tr(recbasis) +
            '. For the cropland of the full District the sum of ' + (fert == 'fertCost' ? 'Cost' : 'Fertilizer') + ' recommended is <b>' +
            (Math.round(app.total * 16 / 100) / 10) + ' ' + (fert == 'fertCost' ? app.tr('coins') : 'tons') + ' ' + app.tr(fert) + '.</b>.').show();
    }, 2500);
}

app.setDefaultVals = function (mode) {
    switch (mode) {
        case 'Low':
            $('#cultivar').val('traditional').change();
            $('#labor').val('manual').change();
            $('#weedcontrol').val('none').change();
            $('#swc').val('none').change();
            break;
        case 'High':
            $('#cultivar').val('high-yielding').change();
            $('#labor').val('mechanized').change();
            $('#weedcontrol').val('optimal').change();
            $('#swc').val('optimal').change();
            break;
        default: //medium (and none)
            $('#cultivar').val('improved').change();
            $('#labor').val('animal').change();
            $('#weedcontrol').val('some').change();
            $('#swc').val('some').change();
            break;
    }
    //also update the default
    ["cultivar", "labor", "weedcontrol", "swc"].forEach(i => app.defaults[i] = $('#' + i + ' option:selected').val());
}

app.setRecText = function (data, mode) {
    //data is the initial graph app.graphData, filter the none response rows
    var rcmdtnData = data.filter(row => (row.id != 'response'));

    //if budget&&field, add a row to rec related to budget
    var budgetperha = 0;
    if ($('#recbasis').val() && parseFloat($('#size').val()) > 0 && parseFloat($('#budget').val()) > 0) {
        budgetperha = parseFloat($('#budget').val()) / parseFloat($('#size').val());
        //does the advice exceed the budget?
        curModeRow = data.filter(row => (row.id == $('#recbasis').val()))[0];
        if (curModeRow['fertCost'] && curModeRow['fertCost'] > budgetperha) {
            withinbudget = Object.assign({}, data.filter(row => (row.id == $('#recbasis').val()))[0]);
        } else {
            //else which row fits the budget?
            var rowinbudget = null;
            all = data.filter(row => (row.id == 'response'));
            all.forEach(i => {
                i['fertCost'] < budgetperha ? rowinbudget = i : null;
            })
            if (rowinbudget) {
                withinbudget = Object.assign({}, rowinbudget);
            }
        }
        withinbudget['id'] = 'withinbudget';
        rcmdtnData.push(withinbudget);
        withinbysize = {}
        withinbysize['id'] = 'Foryourfield';
        ['fNR', 'fPR', 'fKR', 'CNE', 'yfertilized', 'fertCost', 'netReturn'].forEach(i => withinbysize[i] = withinbudget[i] * $('#size').val());
        rcmdtnData.push(withinbysize);
    }
    //rcmdtn = {}
    tblcnt = '<table class="card-body table"><tr><th>Select a fertilizer target' + app.help('Select fertilizer target help') +
        '</th><th class="text-right">N (kg/ha)</th><th class="text-right">P (kg/ha)</th><th class="text-right">K (kg/ha)</th>' +
        '<th class="text-right">CNE (kg/ha)</th><th class="text-right">Yield (kg/ha)</th><th class="text-right">Cost (' +
        app.tr('coins') + '/ha)</th><th class="text-right">Net return (' + app.tr('coins') + '/ha)</th></tr>';
    exn = "", exp = "", exk = "", excne = "", exr = "", exy = "", exf = "", albl = "";
    for (var i in rcmdtnData) {
        albl = (rcmdtnData[i]['id'] in ['withinbudget', 'Foryourfield']) ? app.tr(rcmdtnData[i]['id']) : '<a href="#graph" onclick="app.updateRecGraph(null,null,\'' + rcmdtnData[i]['id'] + '\')">' + app.tr(rcmdtnData[i]['id']) + '</a>';
        if (app.detailData.length > 0) {
            //take the row with matching id
            detRW = app.detailData.filter(row => (row.id == rcmdtnData[i]['id']));
            if (detRW.length > 0) {
                exn = " <span class='text-info'>" + (detRW[0]['fNR'] || 0) + "</span> / ";
                exp = " <span class='text-info'>" + (detRW[0]['fPR'] || 0) + "</span> / ";
                exk = " <span class='text-info'>" + (detRW[0]['fKR'] || 0) + "</span> / ";
                excne = " <span class='text-info'>" + (detRW[0]['CNE'] || 0) + "</span> / ";
                exy = " <span class='text-info'>" + (detRW[0]['yfertilized'] || 0) + "</span> / ";
                exf = " <span class='text-info'>" + (detRW[0]['fertCost'] || 0) + "</span> / ";
                exr = " <span class='text-info'>" + (detRW[0]['netReturn'] || 0) + "</span> / ";
            }
        }

        //rcmdtn[rcmdtnData[i]['id']] = rcmdtnData[i];
        tblcnt += '<tr><td>' + albl + app.help(rcmdtnData[i]['id'] + '_desc') + '</td><td class="text-right">' +
            exn + (rcmdtnData[i]['fNR'] || 0) + '</td><td class="text-right">' +
            exp + (rcmdtnData[i]['fPR'] || 0) + '</td><td class="text-right">' +
            exk + (rcmdtnData[i]['fKR'] || 0) + '</td><td class="text-right">' +
            excne + (rcmdtnData[i]['CNE'] || 0) + '</td><td class="text-right">' +
            exy + (rcmdtnData[i]['yfertilized'] || 0) + '</td><td class="text-right">' +
            exf + (budgetperha > 0 && parseFloat(rcmdtnData[i]['fertCost']) > budgetperha ? '<span class="text-warning" title="' + app.tr('overbudget') + '">' +
                rcmdtnData[i]['fertCost'] + "</span>" : (rcmdtnData[i]['fertCost'] || 0)) + '</td><td class="text-right">' +
            exr + (rcmdtnData[i]['netReturn'] || 0) + '</td></tr>';
        //set per field apart in box    
        if (rcmdtnData[i]['id'] == 'withinbudget') {
            tblcnt += '</table><table class="card-body table mt-2"><tr><th>&nbsp;</th><th class="text-right">N (kg)</th><th class="text-right">P (kg)</th><th class="text-right">K (kg)</th><th class="text-right">CNE (kg)</th><th class="text-right">Yield (kg)</th><th class="text-right">Cost (' + app.tr('coins') + ')</th><th class="text-right">Net return (' + app.tr('coins') + ')</th></tr>';
        }
    }

    $('#RecTxt').html(tblcnt + "</table>");
    $('.help').tooltip();
}

app.help = function (str) {
    return '<span class="help" data-toggle="tooltip" title="' + app.tr(str) + '"></span>';
}

app.tr = function (str) {
    //todo: localize
    if (!trans[str]) {
        console.log('Missing translation: ' + str);
        return str;
    }
    return trans[str];
}

app.updateRecGraph = function (fert, mode, recbasis) {
    if (!recbasis) {
        recbasis = app.get('recbasis');
    } else if (app.app == 'field') {
        $('#recbasis').val(recbasis);
    }
    $('.recmode').html(app.tr(recbasis));
    if (!fert) fert = $("input[name='fert']").val();
    mode = fert == 'fertCost' ? 'netReturn' : 'yfertilized';
    //get recommendations
    var rcmdn = [];
    app.graphData.forEach(r => recbasis == r.id ? rcmdn.push({ "x": r[fert], "y": r[mode], "r": 12, "label": app.tr(r.id) }) : null)
    app.detailData.forEach(r => recbasis == r.id ? rcmdn.push({ "x": r[fert], "y": r[mode], "r": 12, "label": app.tr(r.id) }) : null)
    app.aGraph.data.datasets[1].data = rcmdn;
    app.aGraph.data.datasets[1].label = app.tr(recbasis);
    app.aGraph.update();
}

app.setGraph = function (fert, recbasis) {
    if (recbasis) {
        if (app.app == 'field') { //hidden
            $('#recbasis').val(recbasis);
        } else { //select (by text)
            $('#recbasis option').filter(function () { return $(this).html() == recbasis }).attr('selected', 'true')
        }
    } else {
        if (app.app == 'field') { //hidden
            recbasis = $('#recbasis').val();
        } else { //select (by text)
            recbasis = $('#recbasis option:selected').text();
        }
    }
    mode = fert == 'fertCost' ? 'netReturn' : 'yfertilized';

    //hide the graph, if the value is 0
    total = 0;
    $('#spnXaxis').html((fert == 'fertCost' ? '' : 'Fertilizer ') + app.tr(fert) + ' (' + (fert == 'fertCost' ? app.tr('coins') : 'kg') + '/ha)');
    $('#spnYaxis').html((fert == 'fertCost' ? 'Net return (' : 'Crop yield (') + (fert == 'fertCost' ? app.tr('coins') : 'kg') + '/ha)');
    $('#spnXaxisProb').html(app.tr('Years'));
    $('#spnYaxisProb').html('Net return (' + app.tr('coins') + '/ha)');
    $('#ftDiagramCard').show();
    $('#ftDiagramTxt').hide();

    //builds a graph
    const grData = app.graphData.filter(row => (row.id == 'response'));
    var probData = {};
    //initialize object with arrays for each probability
    probabilities = [];
    probcolors = { '1': 225, '5': 200, '10': 175, '20': 150, '40': 125, '50': 100, '60': 125, '80': 150, '90': 170, '95': 200, '99': 225 };
    //probabilities.forEach(i => probData[i] = []);
    //only records with id='response'; add the values to the relevant probablity

    app.probabilityData.filter(row => (row.id == 'response')).forEach(j => {
        if (!probabilities.includes(j['prob'])) {
            probabilities.push(j['prob']);
            probData[j['prob']] = []
        }
        probData[j['prob']] ? probData[j['prob']].push(j) : null
    });


    //check if result has st-dev
    lastrow = app.graphData.slice(-1).pop();
    hasStDev = lastrow && lastrow['sd_yfertilized'] ? true : false;

    const labels = Array.from(Array(grData.length).keys()) // grData.map(function (row) {return row[fert];})
    const graph = {
        datasets: [{
            label: 'X: ' + app.tr(fert + '-u') + ' Y: ' + app.tr(mode + '-u') + (app.app == 'region' ? ' (District average)' : ''),
            data: grData.map(function (row) {
                if (fert == 'fertCost') {
                    return { "x": row[fert], "y": row[mode] };
                } else if (row['sd_yfertilized']) {
                    return { "x": row[fert], "y": row[mode], "yMin": row[mode] - row['sd_yfertilized'], "yMax": row[mode] + row['sd_yfertilized'], "N": row["fNR"], "P": row["fPR"], "K": row["fKR"] };
                } else {
                    return { "x": row[fert], "y": row[mode], "N": row["fNR"], "P": row["fPR"], "K": row["fKR"] };
                }
            }),
            type: hasStDev ? 'lineWithErrorBars' : 'line',
            cubicInterpolationMode: 'monotone',
            tension: 0.4,
            order: 3,
            borderWidth: 3,
            backgroundColor: 'rgb(255,200,200)',
            borderColor: 'rgb(255,0,0)'
        }, {
            type: 'bubble',
            label: app.tr(app.get('recbasis')),
            data: [],
            order: 1,
            backgroundColor: 'rgb(200,200,200)',
            borderColor: 'rgb(0,0,0)'
        }]
    };

    if (probData) {
        Object.keys(probabilities).forEach(j => {
            i = probabilities[j];
            if (probData[i] && probData[i].length > 0) {
                graph['datasets'].push({
                    label: "Probability " + i,
                    data: probData[i].map(function (row) {
                        return { "x": row[fert], "y": row[mode] };
                    }),
                    type: 'line',
                    cubicInterpolationMode: 'monotone',
                    tension: 0.4,
                    order: 5,
                    backgroundColor: 'rgb(' + probcolors[probabilities[j]] + ',250,' + probcolors[probabilities[j]] + ')',
                    borderColor: 'rgb(' + probcolors[probabilities[j]] + ',230,' + probcolors[probabilities[j]] + ')'
                })
            }
        });
    }

    if (mode == 'netReturn' && parseFloat($('#budget').val()) > 0 && parseFloat($('#size').val()) > 0) {
        var budgetperha = parseFloat($('#budget').val()) / parseFloat($('#size').val());
        var budgetLine = {
            annotations: {
                budgetLine: {
                    type: 'line',
                    xMin: budgetperha,
                    xMax: budgetperha,
                    borderColor: 'rgb(255, 99, 132)',
                    borderWidth: 2,
                }
            }
        }
    } else {
        budgetLine = {};
    }

    const config = {

        data: graph,
        options: {
            responsive: true,
            interaction: {
                mode: 'point',
                intersect: true,
            },
            stacked: false,
            plugins: {
                annotation: budgetLine,
                legend: {
                    display: false
                },
                tooltip: {
                    callbacks: {
                        beforeLabel: function (i) {
                            if (i['raw']['label']) return i['raw']['label'];
                            if (i['raw']['N']) return "N:" + i['raw']['N'] + "; P: " + i['raw']['P'] + "; K: " + i['raw']['K'] + ";"
                        }
                    }
                }
            },
            scales: {
                x: {
                    type: 'linear',
                    position: 'bottom',
                    min: 0
                }

            }
        }

    }

    try {
        app.aGraph.destroy();
    } catch (e) { }

    Chart.defaults.font.size = 16;
    app.aGraph = new Chart(
        document.getElementById('ftDiagram'),
        config
    );
    if (app.detailData.length > 0) {
        graphtype = app.app;

        //remove existing second line first
        if (app.aGraph.data.datasets.length == 4) {
            app.aGraph.data.datasets.pop();
        }
        if (app.aGraph.data.datasets.length == 3) {
            app.aGraph.data.datasets.pop();
        }

        const grData = app.detailData.filter(row => (row.id == 'response'));
        app.aGraph.data.datasets.push({
            label: 'X: ' + app.tr(fert + '-u') + ' Y: ' + app.tr(mode + '-u') + ' (' + (graphtype == 'field' ? 'Hyperlocalized' : 'Selected point') + ')',
            data: grData.map(function (row) {
                return { "x": row[fert], "y": row[mode], "N": row["fNR"], "P": row["fPR"], "K": row["fKR"] };
            }),
            cubicInterpolationMode: 'monotone',
            type: "line",
            borderWidth: 2,
            tension: 0.4,
            order: 2,
            backgroundColor: 'rgb(200, 200, 255)',
            borderColor: 'rgb(99, 99, 200)'
        })

        app.updateRecGraph(fert, mode);
    }
    app.updateRecGraph(fert, mode, recbasis);

    //the probability graph
    if (probData) {
        res = {};
        res2 = [];
        Object.keys(probData).forEach(i => {
            j = probData[i];
            j.forEach(k => {
                if (!res[k['fertCost']]) {
                    res[k['fertCost']] = []
                }
                res[k['fertCost']].push({ 'x': parseFloat(k['prob']), 'y': k['netReturn'] })
            })
        })
        c = 0;
        Object.keys(res).forEach(j => {
            i = res[j];
            res2.push({
                label: 'Cost ' + j + ' ' + app.tr('coins') + '/ha',
                data: i,
                type: 'line',
                cubicInterpolationMode: 'monotone',
                tension: 0.4,
                order: 4,
                backgroundColor: 'rgb(255,255,255)',
                borderColor: app.numberToColorHsl(c++, 11)
            })
        })

    }

    configProb = {
        data: {
            datasets: res2
        },
        options: {
            responsive: true,
            interaction: {
                mode: 'point',
                intersect: false,
            },
            stacked: false,
            plugins: {
                legend: {
                    position: 'right',
                    labels: {
                        font: {
                            size: 14
                        }
                    }
                }
            },
            scales: {
                x: {
                    type: 'linear',
                    position: 'bottom'
                }
            }
        }
    }

    if (app.app == 'field') {
        try {
            app.probGraph.destroy();
        } catch (e) { }

        app.probGraph = new Chart(
            document.getElementById('ftPropDiagram'),
            configProb
        );
    }

}
//}

app.get = function (parameter) {
    if (app.app == 'field') {
        switch (parameter) {
            case 'recbasis':
                return $('#recbasis').val();
            default:

                break;
        }
    } else {
        switch (parameter) {
            case 'recbasis':
                return $('#recbasis option:selected').text();
            default:

                break;
        }
    }
}

app.parseRegionRecommandation = function (data) {
    app.graphData = data;
    app.setGraph('fNR');
    app.setTable(data);
    app.setRecText(data);
}

app.parsePointRecommandation = function (data, recmode) {
    //recmode = field  
    $('.hyper').hide();
    if (recmode == 'field') {
        app.graphData = data[0];
        if (data.length > 1) app.probabilityData = data[1];
        app.setGraph($("input[name='fert']").val());
        app.setTable(app.graphData);
        app.setRecText(app.graphData, recmode);
        //set initial params of hyperlocalise
        app.getParams({ "lat": $('#lat').val(), "lng": $('#lng').val() }, data[2][0]);
    } else if (recmode == 'hyperlocalise') {
        app.detailData = data[0];
        if (data.length > 1) app.probabilityData = data[1];
        app.setGraph($("input[name='fert']").val());
        app.setTable(app.graphData);
        app.setRecText(app.graphData, recmode);
        $('.hyper').show();
    } else { //region
        app.detailData = data[0];
        if (data.length > 1) app.probabilityData = data[1];
        app.setGraph($("input[name='fert']").val());
        app.setTable(app.detailData);
        app.setRecText(app.graphData, recmode);
        $('.hyper').show();
    }
}

app.setTable = function (data) {
    myRows = "";
    theCols = [];
    myCols = "";
    if (data[0]) {
        for (var k in data[data.length - 1]) {
            if (!['x', 'y'].includes(k)) {
                theCols.push(k);
                myCols += "<th scope='col'>" + app.tr(k) + "</th>";
            }
        }

        $(data).each(function (i) {
            myRows += "<tr>";
            for (var k in theCols) {
                myRows += "<td>" + (this[theCols[k]] ? this[theCols[k]] : "0") + "</td>";
            }
            myRows += "</tr>";
        })
        $('#thead').html(myCols);
        $('#tbody').html(myRows);
    }
}

app.activeTab = function (tab) {
    $('.nav-pills a[href="#' + tab + '"]').tab('show');
};

app.distance = function (o,p) {
    return Math.sqrt(Math.pow(o.lat - p.lat, 2) + Math.pow(o.lng - p.lng, 2))
}

app.selectRegionfromMap = function (evt) {
    shortest = 1000;
    var latlng = app.map.mouseEventToLatLng(evt.originalEvent);
    prov0 = ''
    dist0 = ''
    for (const [k1, v1] of Object.entries(app.districts)) {
        for (const [k2, v2] of Object.entries(v1)) {
            d0 = app.distance(v2.getCenter(),latlng)
            if (d0 < shortest) {
                shortest = d0;
                prov0 = k1;
                dist0 = k2;
            }
        }
    }
    $("#province").val(prov0).change();
    $("#district").val(dist0).change();
}

app.clickRegionMap = function (evt) {
    var latlng = app.fertMap.mouseEventToLatLng(evt.originalEvent);
    if (app.regionMarker != {}) app.fertMap.removeLayer(app.regionMarker);
    app.regionMarker = new L.Marker(latlng).addTo(app.fertMap);
    $('#lng').val(Math.round(latlng.lng * 10000) / 10000);
    $('#lat').val(Math.round(latlng.lat * 10000) / 10000);
    app.fetchPointRecommandation('region');
}

// ################################################
// ########            Field app           ########
// ################################################

app.loadFieldApp = function () {
    //app.markerHistory = L.markerClusterGroup();
    app.map = L.map('map', { minZoom: 6, maxZoom: 19 }).setView([9, 40], 8);
    L.control.scale({ imperial: false }).addTo(app.map);
    var bing = new L.BingLayer(app.BING_KEY);
    app.map.addLayer(bing);
    wmsMaskLayer = L.tileLayer.wms(app.wmsurl + app.currentCountry, { layers: 'mask', transparent: true, opacity: .5, format: "image/png", tileSize: 1024 }).addTo(app.map);
    wmsLayer = L.tileLayer.wms(app.wmsurl + app.currentCountry, { layers: 'adm3', transparent: true, format: "image/png", tileSize: 1024 }).addTo(app.map);

    L.Control.XYpanel = L.Control.extend({
        onAdd: function () {
            var el = L.DomUtil.create('div', 'xy-panel');
            L.DomEvent.disableClickPropagation(el);
            htm = '<div class="input-group input-group-sm"><div class="input-group-prepend"><span class="input-group-text">' +
                '<img src="/images/locate.png" title="Retrieve your current position" style="width:20px" onclick="app.locate()"></span></div>' +
                '<input type="number" step="0.001" class="form-control" aria-label="longitude (x)" id="lng" style="width:70px" placeholder="longitude" min="5" max="15">' +
                '<input type="number" step="0.001" class="form-control" aria-label="latitude (y)" id="lat" style="width:70px" placeholder="latitude" min="30" max="50">' +
                '<button class="form-control" onclick="app.zoom()" style="width:50px" id="btGo"> Go </button></div>';
            el.innerHTML = htm;
            return el;
        },
        onRemove: function () { }
    });

    L.control.xypanel = function (opts) {
        return new L.Control.XYpanel(opts);
    }

    L.control.xypanel({
        position: 'topright'
    }).addTo(app.map);

    //bg1 = L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', { maxZoom: 19, attribution: '© OpenStreetMap' }).addTo(app.map);
    app.histMap = L.map('histMap', { minZoom: 6, maxZoom: 19 }).setView([9, 40], 8);
    L.control.scale({ imperial: false }).addTo(app.histMap);
    var bing2 = new L.BingLayer(app.BING_KEY);
    app.histMap.addLayer(bing2);

    app.map.on('click', function (evt) {
        var latlng = app.map.mouseEventToLatLng(evt.originalEvent);
        app.setLocation(latlng);
    });
    app.plotHistory();
    app.loadHelpLabels();
}

// ################################################
// ########            Region app           #######
// ################################################

app.loadRegionApp = function () {

    app.map = L.map('districtMap', { minZoom: 6, maxZoom: 19 }).setView([9, 40], 8);
    L.control.scale({ imperial: false }).addTo(app.map);
    app.fertMap = L.map('fertMap', { minZoom: 6, maxZoom: 19 }).setView([9, 40], 8);
    L.control.scale({ imperial: false }).addTo(app.fertMap);

    var bing = new L.BingLayer(app.BING_KEY);
    app.map.addLayer(bing);

    var bing2 = new L.BingLayer(app.BING_KEY);
    app.fertMap.addLayer(bing2);

    wmsLayer = L.tileLayer.wms(app.wmsurl + app.currentCountry, { layers: 'adm3', transparent: true, format: "image/png", tileSize: 1024 }).addTo(app.map);
    wmsLayer2 = L.tileLayer.wms(app.wmsurl + app.currentCountry, { layers: 'adm3', transparent: true, format: "image/png", tileSize: 1024 }).addTo(app.fertMap);

    app.msl = [];
    overlays = {};

    //app.countryLayers();

    baselayers = { "Aerial": bing }

    //var layerControl = L.control.layers(baselayers).addTo(app.map);

    app.map.on('click', function (evt) {
        app.selectRegionfromMap(evt);
    });

    app.fertMap.on('click', function (evt) {
        app.clickRegionMap(evt);
    });

    L.Control.Mapmode = L.Control.extend({
        onAdd: function () {
            var el = L.DomUtil.create('div', 'lyrsPanel bg-white rounded');
            L.DomEvent.disableClickPropagation(el);
            htm = '<select class="custom-select" id="recbasis" onchange="app.setRegionMap()" title="' + app.tr('rec-base') + '">';
            ["default", "maxFertEff", "maxNetReturn", "maxNetValueCostRatio"].forEach(rb => htm +=
                '<option value="' + rb + '">' + app.tr(rb) + '</option>');
            htm += '</select><div class="list-group" id="fertChoice">';
            ["fNR", "fPR", "fKR", "fertCost"].forEach(rb => htm +=
                '<a class="list-group-item list-group-item-action ' + (rb == 'fNR' ? 'active' : '') +
                '" data-toggle="list" href="#" onclick="app.setRegionMap(\'' + rb + '\')" role="tab" >' + app.tr(rb) + '</a>');
            htm += '</div>';
            el.innerHTML = htm;
            return el;
        },
        onRemove: function (wmsLayer,) { }
    });

    L.control.mapmode = function (opts) {
        return new L.Control.Mapmode(opts);
    }

    L.control.mapmode({
        position: 'topright'
    }).addTo(app.fertMap);

    L.Control.Maplegend = L.Control.extend({
        onAdd: function () {
            var el = L.DomUtil.create('div', 'leaflet-bar mapLegend p-1');
            el.innerHTML = '';
            return el;
        },
        onRemove: function () { }
    });

    L.control.maplegend = function (opts) {
        return new L.Control.Maplegend(opts);
    }

    L.control.maplegend({
        position: 'bottomleft'
    }).addTo(app.fertMap);

    app.loadHelpLabels();
}


app.countryLayers = function () {
    app.msl = [];
    overlays = {};
    app.lyrs.forEach(l => app.msl[l] = L.tileLayer.wms(app.wmsurl + app.currentCountry, { layers: 'adm3', transparent: true, format: "image/png" }));
    app.lyrs.forEach(l => overlays[app.tr(l)] = app.msl[l]);
}

app.locate = function () {
    app.map.locate({ setView: false });
    app.map.on('locationfound', app.onLocationFound);
}

app.onLocationFound = function (e) {
    app.setLocation(e.latlng);
};

app.setLocation = function (latlng) {
    $("#lat").val(Math.round(latlng.lat * 10000) / 10000);
    $("#lng").val(Math.round(latlng.lng * 10000) / 10000);
    app.setMarker(latlng);
}

app.zoom = function (x, y, z) {
    if (!x) x = $("#lng").val();
    if (!y) y = $("#lat").val();
    if (!z) z = 18;
    app.setMarker([y, x]);
    app.map.setView([y, x], z);
}

app.setMarker = function (latlng) {
    //(un)set marker
    if (app.marker != {}) app.map.removeLayer(app.marker);
    app.marker = new L.Marker(latlng).addTo(app.map);
    app.map.setView(latlng, 18)
}

//default prices are returned by model
app.loadDefaultPrices = function () {
    if (app.currentCountry != '' && app.countries[app.currentCountry]) {
        if (app.countries[app.currentCountry].fertilisers) {
            for (const [key, value] of Object.entries(app.countries[app.currentCountry].fertilisers)) {
                if (value.price && value.price > 0) { $('#costf' + key + 'R').val(value.price) }
            }
        }
        if ($('#crop').val() != '' && app.countries[app.currentCountry].crops &&
            app.countries[app.currentCountry].crops[$('#crop').val()]) {
            $('#prizeMarket').val(app.countries[app.currentCountry].crops[$('#crop').val()][0]);
        }
    }
}

//load balloon content of help icons from trans.js
app.loadHelpLabels = function () {
    $('.help').each(function (i) {
        $(this).prop('title', app.tr($(this).prop('title')));
    }).tooltip();
}

app.numberToColorHsl = function (i, steps) {
    // as the function expects a value between 0 and 1, and red = 0° and green = 120°
    // we convert the input to the appropriate hue value
    var hue = (i / steps) * 120 / 360;
    // we convert hsl to rgb (saturation 100%, lightness 50%)
    var rgb = app.hslToRgb(hue, 1, .5);
    // we format to css value and return
    return 'rgb(' + rgb[0] + ',' + rgb[1] + ',' + rgb[2] + ')';
}

app.hslToRgb = function (h, s, l) {
    var r, g, b;

    if (s == 0) {
        r = g = b = l; // achromatic
    } else {
        function hue2rgb(p, q, t) {
            if (t < 0) t += 1;
            if (t > 1) t -= 1;
            if (t < 1 / 6) return p + (q - p) * 6 * t;
            if (t < 1 / 2) return q;
            if (t < 2 / 3) return p + (q - p) * (2 / 3 - t) * 6;
            return p;
        }

        var q = l < 0.5 ? l * (1 + s) : l + s - l * s;
        var p = 2 * l - q;
        r = hue2rgb(p, q, h + 1 / 3);
        g = hue2rgb(p, q, h);
        b = hue2rgb(p, q, h - 1 / 3);
    }

    return [Math.floor(r * 255), Math.floor(g * 255), Math.floor(b * 255)];
}

// ################################################
// ##########          farm app          ##########
// ################################################

app.loadFarmApp = function () {
    //bg1 = L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', { maxZoom: 19, attribution: '© OpenStreetMap' }).addTo(app.map);
    app.map = L.map('farmMap', { minZoom: 6, maxZoom: 19 }).setView([9, 40], 8);
    L.control.scale({ imperial: false }).addTo(app.map);
    var bing2 = new L.BingLayer(app.BING_KEY);
    app.map.addLayer(bing2);

    wmsLayer = L.tileLayer.wms(app.wmsurl + app.currentCountry, { layers: 'adm3', transparent: true, format: "image/png", tileSize: 1024 }).addTo(app.map);

    app.farmFields = new L.FeatureGroup();
    app.map.addLayer(app.farmFields);

    var drawPluginOptions = {
        position: 'topright',
        draw: {
            polygon: {
                allowIntersection: false, // Restricts shapes to simple polygons
                drawError: {
                    color: '#e1e100', // Color the shape will turn when intersects
                    message: 'No overlap.' // Message that will show when intersect
                },
                shapeOptions: {
                    color: '#97009c'
                }
            },
            // disable toolbar item by setting it to false
            polyline: false,
            circle: false, // Turns off this drawing tool
            rectangle: false,
            marker: false,
        },
        edit: {
            featureGroup: app.farmFields, //REQUIRED!!
            remove: false
        }
    };

    // Initialise the draw control and pass it the FeatureGroup of editable layers
    var drawControl = new L.Control.Draw(drawPluginOptions);
    app.map.addControl(drawControl);

    app.map.on('draw:created', function (e) {
        var type = e.layerType, layer = e.layer;
        app.farmFields.addLayer(layer);
        if (!app.farm.id || app.farm.id == '') {
            alert('Please select or create a farm first');
        } else {
            app.fieldsUpdate();
            app.setFarm(); //save farm to localstorage
        }
    });

    app.getFarms();
    //app.plotHistory();
    app.loadHelpLabels();
}

app.getFarms = function () {
    app.farms = JSON.parse(localStorage.getItem('farms'));
    if (!app.farms) app.farms = {};
    app.setFarmSelect();
}

app.setFarmSelect = function () {
    $("#farms").find('option').remove().end();
    $("#farms").append($('<option>', {
        value: '',
        text: 'Select farm'
    }));
    for (const [key, val] of Object.entries(app.farms).sort()) {
        $("#farms").append($('<option>', {
            value: key,
            text: val.label
        }));
    }
    if (app.farm && app.farm.id) {
        $('#farms').val(app.farm.id);
        $('#btnFarmEdit').show();
    } else {
        $('#btnFarmEdit').hide();
    }
}

app.setFarm = function () {
    app.farm.fields = {}
    app.farmFields.eachLayer(l => {
        afld = {}
        try {
            afld = { 'geo': l.toGeoJSON() };
        } catch (e) { console.log('failed extracting geometry') }
        if (!l.fid || l.fid == '') { //new fields
            l.fid = Math.round(Math.random() * 10000);
        }
        ["fid", "name", "crop", "intensity"].forEach(p => {
            afld[p] = (l[p] ? l[p] : '')
        });
        app.farm.fields['f' + l.fid] = afld;
    })
    console.log('set farm', app.farm)
    localStorage.setItem('farm' + app.farm.id, JSON.stringify(app.farm));
}

app.setFarms = function () {
    console.log('set farms', app.farms)
    localStorage.setItem('farms', JSON.stringify(app.farms));
}

app.farmSave = function () {
    if ($('#farmLabel').val().trim() == '') {
        alert('No empty label');
    } else {
        //create farm if new else update
        if (!app.farm.id) {
            app.farm.id = crypto.randomUUID();
            app.farm.fields = {};
        }
        app.farm = {
            'id': app.farm.id,
            'label': $('#farmLabel').val(),
            'desc': $('#farmDesc').val(),
            'icon': $('#selectIcon').val(),
            'budget': $('#farmBudget').val(),
            'country': $('#country').val(),
            'fields': app.farm.fields
        }
        app.farms[app.farm.id] = { 'label': app.farm.label }
        app.setFarms();
        $('.farmLabel').html(app.farm.label);
        //set app.farm from form
        app.setFarm();
        //if new/updated, update farm select
        app.setFarmSelect();
        //update farm ui
        app.updateFarmUI();
        alert('farm saved');
    }
}

app.farmOpen = function (id) {
    //set farm details
    app.farm = JSON.parse(localStorage.getItem('farm' + id));
    if (!app.farm) {
        app.farm = {
            'id': id,
            'label': '',
            'desc': '',
            'icon': '',
            'budget': '',
            'country': '',
            'fields': {}
        }
    }
    app.updateFarmUI();
    app.openFieldsFromFarm();
    if (app.farmFields && app.farmFields.getBounds().isValid()){
        try {
            app.map.fitBounds(app.farmFields.getBounds()); //zoom to farm 
        } catch(e) {
            console.log(e);
        } 
    }
}

app.farmDelete = function () {
    localStorage.removeItem('farm' + app.farm.id);
    delete app.farms[app.farm.id]
    app.farm = {}
    this.setFarms();
    app.farmFields.eachLayer(l => { app.farmFields.removeLayer(l) });
    app.setFarmSelect();
}

app.updateFarmUI = function () {
    if (app.farm.id && app.farm.id != '') {
        $('#btnFarmEdit').show();
        $('#dvFarmDesc').html('<i class="fa fa-2xl px-2 py-3 rounded-circle m-2 float-right bg-primary text-white fa-' + (app.farm.icon || 'lemon') + '"></i><b class="h3">' + app.farm.label + '</b><br>' +
            app.farm.desc + '<br/><br/><b>Budget:</b> ' + app.farm.budget + ' ' + app.tr('coins') + '.');
    } else {
        $('#btnFarmEdit').hide();
        $('#dvFarmDesc').html('');
    }
    //set farm details
    if ($('#country').val() != app.farm.country) { $('#country').val(app.farm.country).change() }
    $('#farmLabel').val(app.farm.label);
    $('#farmDesc').val(app.farm.desc);
    $('#farmBudget').val(app.farm.budget);
    $('.farmLabel').html(app.farm.label);
    $('#selectIcon').html(app.setIconSelect()).val(app.farm.icon ? app.farm.icon : '');
}

app.fieldsUpdate = function () {
    myFields = "";
    app.farmFields.eachLayer(l => {
        if (!l.fid || l.fid == '') l.fid = Math.round(Math.random() * 10000);
        k = l.fid.toString();
        bsFlds = "<td><select class='form-control form-control-sm crop' id=c_" + k + "></select></td>" +
            "<td><select class='form-control form-control-sm intensity' id=m_" + k + "><option>Low</option><option>Medium</option><option>High</option></select></td>" +
            "<td><button class='btn btn-sm btn-warning' id=r_" + k + ">" + app.tr('remove') + "</button>" +
            "<button class='btn btn-sm btn-primary ml-1' id=h_" + k + ">" + app.tr('hyperlocalize') + "</button></td>";
        ["name", "crop", "intensity"].forEach(p => { (!l[p]) ? l[p] = '' : null }); //if new layer
        myFields += "<tr><td>" + k + "</td><td><input type=text id=n_" + k + " class='form-control form-control-sm' value='" + l.name + "'></td>" + bsFlds + "</tr>";
    });
    $('#fieldPanel').html(myFields);
    app.setCrops('select.crop', $('#country').val()); //todo: also on country change
    app.setFieldSelects(); //first build selects, then populate them, then set them
    $('#fieldPanel input').change(app.changeField);
    $('#fieldPanel select').change(app.changeField);
    $('#fieldPanel button').click(app.fieldAction);
}

app.setFieldSelects = function () {
    app.farmFields.eachLayer(l => {
        k = l.fid.toString();
        $('#c_' + k).val(l.crop);
        $('#m_' + k).val(l.intensity);
    })
}

app.changeField = function (a) {
    ti = a.target.id.split('_');
    t = ti[0]; i = ti[1];
    app.farmFields.eachLayer(l => {
        if (l.fid.toString() === i) {
            l[t == 'c' ? 'crop' : t == 'm' ? 'intensity' : 'name'] = a.target.value;
        }
    })
    app.setFarm();
}

app.fieldAction = function (a) {
    ti = a.target.id.split('_');
    t = ti[0]; i = ti[1];
    if (t == 'r') {
        app.farmFields.eachLayer(l => {
            if (l.fid == i) {
                app.farmFields.removeLayer(l);
            }
        });
        app.fieldsUpdate();
        app.setFarm();
    } else if (t == 'h') {
        alert('Hyperlocalize is not available yet')
    }
}

app.openFieldsFromFarm = function () {
    //remove existing
    app.farmFields.eachLayer(l => { app.farmFields.removeLayer(l) });
    Object.values(app.farm.fields).forEach(l => {
        if (typeof l === 'object' && l && l.geo) {
            aLyr = L.geoJSON(l.geo);
            if (!l.fid || l.fid == '') l.fid = Math.round(Math.random() * 10000);
            ["fid", "name", "crop", "intensity"].forEach(p => { aLyr[p] = l[p] ? l[p] : '' });
            app.farmFields.addLayer(aLyr);
        }
    })
    //update the table
    app.fieldsUpdate()
}

app.setIconSelect = function (item) {
    icons = "";
    ["lemon", "cat", "crow", "house-chimney-window", "dove", "sun-plant-wilt", "hippo", "mosquito", "leaf", "seedling", "fish", "cow", "horse", "worm", "dog", "mountain", "wind", "raindrops", "feather", "tractor", "cheese", "wheat-awn"].forEach(
        i => icons += '<option value=' + i + '>' + i + '</option>');
    return icons;
}

app.farmoptimizer = function () {
    $('#loading').show();
    $.ajax({
        type: "POST",
        url: app.modelurl + 'farm/' + app.farm.id,
        contentType: "application/json; charset=utf-8",
        dataType: 'json',
        data: JSON.stringify({
            country: app.currentCountry,
            fields: Object.values(app.farm.fields),
            fertilisers: Object.entries(app.countries[app.currentCountry].fertilisers).map(e => {
                [key, value] = e; value.name = key; return value
            }),
            configs: Object.entries(app.countries[app.currentCountry].crops).map(e => {
                [key, value] = e; value.name = key; return value
            })
        }),
        beforeSend: function (xhr) {
            xhr.setRequestHeader("Authorization", "Basic " + btoa($('#us').val() + ":" + $('#pw').val()));
        },
        success: function (json) {
            app.farm.results = json;
            app.updateResults();
            app.activeTab('Results');
        },
        error: function (jq, err, t) {
            try {
                app.error('Error: ' + jq.responseJSON.message[0]);
            } catch (e) {
                app.error('error', e)
            }
        },
        complete: function () {
            $('#loading').hide();
        }
    })
}

app.importFarm = async function (){
    //alert('Not implemented yet');
    const file = document.getElementById('formFileSm').files.item(0)
    const text = await file.text();
    json = JSON.parse(text);
    app.farm = json;
    app.updateFarmUI();
    alert(text)
}

app.exportFarm = function (){
    if(app.farm && app.farm.label){
        app.saveJSON(app.farm,app.farm.label+'.json');
    } else {
        alert('Create farm label first');
    }
}

app.saveJSON = function (content,fileName) {
    var a = document.createElement("a");
    var file = new Blob([JSON.stringify(content)], {type: 'text/plain'});
    a.href = URL.createObjectURL(file);
    a.download = fileName;
    a.click();
}

app.updateResults = function () {
    res = app.farm.results.fields.map(f => `<tr>
    <td> ${f.name} </td><td> ${Math.round(Math.random() * 10000)}</td>
    <td> ${f.crop} </td>
    <td> ${f.intensity} </td></tr>`);
    $('#results-body').html(res.join(''));
}

app.test = function () {
    var urls = [
        "Maize/Medium?country=Niger&x=5.4791&y=14.0287&prizeMarket=131.56&costfNR=247&costfPR=750.75&costfKR=269.75",
        "Maize/High?country=Niger&x=12.698&y=13.8627&prizeMarket=263.12&costfNR=494&costfPR=1501.5&costfKR=539.5",
        "Sorghum/High?country=Madagascar&x=44.4659&y=-24.0858&prizeMarket=1000.224&costfNR=3442.8&costfPR=10464.3&costfKR=3759.9",
        "Maize/Medium?country=Madagascar&x=44.4448&y=-24.0724&prizeMarket=916.872&costfNR=3442.8&costfPR=10464.3&costfKR=3759.9",
        "Rice/Low?country=Madagascar&x=44.6396&y=-22.0903&prizeMarket=1083.576&costfNR=3442.8&costfPR=10464.3&costfKR=3759.9",
        "Cassava/High?country=Madagascar&x=44.6396&y=-22.0903&prizeMarket=1083.576&costfNR=3442.8&costfPR=10464.3&costfKR=3759.9",
        "Rice/High?country=Malawi&x=34.783&y=-16.015&prizeMarket=245.4192&costfNR=389.88&costfPR=1185.03&costfKR=425.79",
        "Cassava/High?country=Malawi&x=34.783&y=-16.015&prizeMarket=245.4192&costfNR=389.88&costfPR=1185.03&costfKR=425.79&Fallow=true&Leguminous=false&Mineral=true&cultivar=traditional&labor=animal&weedcontrol=some&swc=&texture=C&BulkDens=&Corg=&Kexch=&Norg=&Polsen=-30&Ptot=212&pH=&rzd=23",
        "Wheat/High?country=Ethiopia&x=42.0237&y=9.6033&prizeMarket=21.252&costfNR=20.9&costfPR=63.525&costfKR=22.825",
        "Barley/Medium?country=Ethiopia&x=42.0226&y=9.6026&prizeMarket=21.252&costfNR=20.9&costfPR=63.525&costfKR=22.825&Fallow=false&Leguminous=true&Mineral=true&cultivar=&labor=&weedcontrol=&swc=&texture=SCL&BulkDens=&Corg=4&Kexch=&Norg=4&Polsen=&Ptot=4&pH=&rzd=200",
        "Cassava/High?country=Tanzania&x=31.7497&y=-6.3999&prizeMarket=659.088&costfNR=907.44&costfPR=2758.14&costfKR=991.02",
        "Cassava/High?country=Tanzania&x=31.7497&y=-6.3999&prizeMarket=659.088&costfNR=907.44&costfPR=2758.14&costfKR=991.02&Fallow=false&Leguminous=true&Mineral=true&cultivar=&labor=&weedcontrol=some&swc=&texture=&BulkDens=4&Corg=2&Kexch=&Norg=3&Polsen=&Ptot=&pH=&rzd=",
        "Maize/High?country=Tanzania&x=33.6311&y=-9.2453&prizeMarket=483.3312&costfNR=907.44&costfPR=2758.14&costfKR=991.02",
        "Teff/High?country=Ethiopia&x=37.2797&y=13.7417&prizeMarket=21.252&costfNR=20.9&costfPR=63.525&costfKR=22.825",
        "Barley/High?country=Ethiopia&x=37.2797&y=13.7417&prizeMarket=21.252&costfNR=20.9&costfPR=63.525&costfKR=22.825"
    ];
    //todo: run each against the model, see if successfull
    urls.forEach(u => {
        $.ajax({
            type: "GET",
            url: app.modelurl + "yieldresponse_hyper/" + u,
            dataType: "json",
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Authorization", "Basic " + btoa($('#us').val() + ":" + $('#pw').val()));
            },
            success: function (json) {
                try {
                    //console.log('success', u, json[0][0]['netReturn']);
                } catch (e) {
                    console.log('content fail', u, e);
                }
            },
            error: function (json, err, t) {
                console.log('http fail', u, json.responseJSON.message || error );
            }
        })
    })
    return true;
}

//prevent to submit form when hitting enter
$(document).ready(function () {
        $(window).keydown(function (event) {
            if (event.keyCode == 13) {
                event.preventDefault();
                return false;
            }
        });
    });

