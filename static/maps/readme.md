# shape to csv

shapes downloaded from https://data.humdata.org/dataset/cod-ab-mdg

converted to csv based on centroid with ogr

ogr2ogr -sql "SELECT ST_PointOnSurface(geometry), adm_03,adm_02,adm_01 FROM ner_adm03_feb2018" -dialect sqlite -f CSV output.csv ner_adm03_feb2018.shp -lco GEOMETRY=AS_XY