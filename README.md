
![space2place](https://git.wur.nl/uploads/-/system/project/avatar/11799/space2place.png)

# Space 2 Place portal

A web based portal for the space 2 place community, dev-url is https://dev.space2place.containers.wurnet.nl, prod (not deployed yet) https://space2place.containers.wurnet.nl

## Technology

The site is [Hugo](https://gohugo.io) based. Herotheme with bootstrap. Read more about `hugo at isric` in the [lsc-hubs project](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/-/blob/master/README.md).

The portal includes a javascript app connected to the fertilization model, it can assess optimal yield conditions based on predicted or observed environment properties. The app is build using the jquery, leaflet & chart.js libraries.

Read more in [documentation](https://git.wur.nl/isric/landpks/docs).

## Users

the model on prod is protected by a password, with firefox navigate to https://ifdc@api.space2place.isric.org, fill in credentials, then go to https://space2place.isric.org

- usaid:Whereisthesun7

## Set up basic auth

See [article](https://git.wur.nl/isric/ict/it.related/-/blob/master/Methods/K8S-WUR/K8sIngress.md#password-realm-auth)

set relevant namespace `isric-data-prod`
```
kubectl create configmap cm-files-landpks --from-file=./overlays/prod/config --dry-run=client -o yaml --namespace=isric-data-prod > ./overlays/prod/config-map.yaml
```

## Releases

- 2023-10-17 Add Zambia